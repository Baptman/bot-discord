const {Client, Collection} = require('discord.js');
const config = require('./config.json');

const client = new Client();


client.commands = new Collection();

client.commands.set("type",require("./commands/type.js"));
client.commands.set("stats",require("./commands/stats.js"));
client.commands.set("evolution",require("./commands/evolution.js"));
client.commands.set("combat",require("./commands/combat.js"));
client.commands.set("help",require("./commands/help.js"));
client.commands.set("traduction",require("./commands/traduction.js"));

// Informe que le bot est en ligne
client.on('ready', function(){
  require('./events/ready.js')(client);
});

client.on("message", function(message){
  require("./events/message.js")(client, message);
});

client.login(config.BOT_TOKEN);
