const {Wit ,log} = require('node-wit');

module.exports = function(client, message){
  const prefix = "!";
  var args = "";
  if(!message.content.startsWith(prefix)) return;
  const commandBody = message.content.slice(prefix.length);
  const wit = new Wit ({ accessToken : '7F7USKR6USS7SO7IT5DMIJ3HFTDVUZUI' });
  wit.message(message, {})
  .then ((data) => {
  if(data.entities['pokemon:pokemon']){
    const commande = data.intents[0].name.toLowerCase();
    if(commande == "combat"){
      args = [data.entities['pokemon:pokemon'][0].value,data.entities['pokemon:pokemon'][1].value];
    }else if(commande == "traduction"){
      args = [data.entities['pokemon:pokemon'][0].value,data.entities['langue:langue'][0].value];
    }else if(commande != "help"){
      args = data.entities['pokemon:pokemon'][0].value;
    }
    if(client.commands.has(commande)){
      console.log('Commande : ' + commande);
      client.commands.get(commande)(client,message,args);
    }
  }else{
    message.reply(`Je n'ai pas compris votre demande, Merci de bien vouloir recommencer !`);
  }
}).catch((data) => {
  console.error;
  message.reply(`Je n'ai pas compris votre demande, Merci de bien vouloir recommencer !`);
  });
};
