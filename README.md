# Bot Discord connecté à l'API Pokemon GO

- Pour lancer le bot, il faut faire en premier lieu faire "npm install" qui installera les packages nécessaires.
- Voici les packages utilisés par le projet : "discord.js", "node-wit", "pokemon", "unirest".
- Pour executer le projet, il faut faire la commande "node bot.js".

# Informations d'utilisations

- Le bot utilise le machine learning grâce à l'API node-wit, cela n'est pas totalement au point donc il se peut qu'il ne comprenne pas à des moments.
- Pour rechercher un pokémon, il faut soit utiliser son nom anglais, soit utiliser son numéro dans le pokedex.

# BOT Discord fait par Erwan CARNEIRO, Baptiste DUVAL.
