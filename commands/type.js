const unirest = require("unirest");

const req = unirest("GET", "https://pokemon-go1.p.rapidapi.com/pokemon_types.json");

req.headers({
	"x-rapidapi-key": "d2a1cf1847msh7cb897edaba547bp1b6952jsnde9cc7dce47e",
	"x-rapidapi-host": "pokemon-go1.p.rapidapi.com",
	"useQueryString": true
});

module.exports = function(client, message, args){
  req.end(function (res) {
    let connu = false;
  	if (res.error) throw new Error(res.error);
    console.log(args);
    for(const pokemon of res.body){
      let pokemon_id = pokemon['pokemon_id'].toString();
      let types = pokemon['type'];
      let pokemon_types;
      if(pokemon['pokemon_name'].toString().toLowerCase() === args.toString().toLowerCase() && !connu || pokemon_id == args && !connu){
        while(pokemon_id.length < 3){
          pokemon_id = "0" + pokemon_id;
        }
        console.log(types);
        for(const type of types){
          pokemon_types = ' ' + type.value;
        }
        console.log(pokemon_id);
        message.reply("https://assets.pokemon.com/assets/cms2/img/pokedex/full/" + pokemon_id + ".png\n" +
        pokemon['pokemon_name'] + ` est le pokémon n°` + pokemon_id + ` du pokédex !
        Il est de type : ` +
          types
      );

      connu = true;
      }
    }
    if(!connu){
      message.reply(args + ` n'existe pas !`);
    }
  });
}
