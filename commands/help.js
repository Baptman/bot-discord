module.exports = function(client, message, args){
  if(args == ''){
    message.reply(`https://www.pokepedia.fr/images/0/0c/Professeur_Chen-LGPE.png
    Bonjour, je suis le professeur Chen et je suis là pour t'aider !
    Voici les différentes possibilités que je vous propose !
    Liste des possibilités :
        - **evolution** | voir les évolutions d'un pokémon !
        - **type** | voir les types d'un pokémon !
        - **stats** | voir les statistiques d'un pokémon !
        - **combat** | faire combattre deux pokémons !
        - **traduction** | traduire le nom d'un pokemon !
    (Vous devez écrire soit le nom du pokémon en anglais, soit son numéro dans le pokedex lors de votre recherche)
    `);
  }
}
