const unirest = require("unirest");
const {Client, Collection} = require('discord.js');

const req = unirest("GET", "https://pokemon-go1.p.rapidapi.com/pokemon_stats.json");

req.headers({
	"x-rapidapi-key": "d2a1cf1847msh7cb897edaba547bp1b6952jsnde9cc7dce47e",
	"x-rapidapi-host": "pokemon-go1.p.rapidapi.com",
	"useQueryString": true
});

module.exports = function(client, message, args){
  req.end(function (res) {
    let connu = false;
    let connu1 = false;
  	if (res.error) throw new Error(res.error);
    let pokemon1 = "";
    let pokemon2 = "";
    console.log(args.length);
    if(args.length == 2){
      for(const pokemon of res.body){
        let pokemon_id = pokemon['pokemon_id'].toString();
        if(pokemon['pokemon_name'].toString().toLowerCase() === args[0].toString().toLowerCase() && !connu || pokemon_id == args[0] && !connu){
          pokemon1 = pokemon;
          while(pokemon_id.length < 3){
            pokemon_id = "0" + pokemon_id;
          }
          console.log(pokemon_id + " " + pokemon['pokemon_name']);
          message.reply("https://assets.pokemon.com/assets/cms2/img/pokedex/full/" + pokemon_id + ".png\n" +
          pokemon['pokemon_name'] + ` est le pokémon n°` + pokemon_id + ` du pokédex !
          Ces stats de bases sont :
            Attaque ` + pokemon['base_attack'] + `
            Defense ` + pokemon['base_defense'] + `
            PV ` + pokemon['base_stamina']);
        connu = true;
        }
        if(pokemon['pokemon_name'].toString().toLowerCase() === args[1].toString().toLowerCase() && !connu1 || pokemon_id == args[1] && !connu1){
          pokemon2 = pokemon;
          while(pokemon_id.length < 3){
            pokemon_id = "0" + pokemon_id;
          }
          console.log(pokemon_id + " " + pokemon['pokemon_name']);
          message.reply("https://assets.pokemon.com/assets/cms2/img/pokedex/full/" + pokemon_id + ".png\n" +
          pokemon['pokemon_name'] + ` est le pokémon n°` + pokemon_id + ` du pokédex !
          Ces stats de bases sont :
            Attaque ` + pokemon['base_attack'] + `
            Defense ` + pokemon['base_defense'] + `
            PV ` + pokemon['base_stamina']);
        connu1 = true;
        }
      }
      let pokemon1PV = pokemon1['base_stamina'];
      let pokemon2PV = pokemon2['base_stamina'];
      while(pokemon1PV >= 0 && pokemon2PV >= 0){
        pokemon1PV -= Math.floor(((Math.random() * (pokemon2['base_attack']/2))/2)*(pokemon2['base_attack']/pokemon1['base_attack']))+1;
        pokemon2PV -= Math.floor(((Math.random() * (pokemon1['base_attack']/2))/2)*(pokemon1['base_attack']/pokemon2['base_attack']))+1;
        message.channel.send(pokemon1['pokemon_name'] + ` a ` + pokemon1PV + ` PV restant\n` +
        pokemon2['pokemon_name'] + ` a ` + pokemon2PV + ` PV restant`);
      }
      if(pokemon1PV <= 0 && pokemon2PV > 0){
        message.reply(pokemon2['pokemon_name'] + ` remporte le combat contre ` + pokemon1['pokemon_name'] + ` !`);
      }else if(pokemon2PV <= 0 && pokemon1PV > 0){
        message.reply(pokemon1['pokemon_name'] + ` remporte le combat contre ` + pokemon2['pokemon_name'] + ` !`);
      }else if(pokemon2PV <= 0 && pokemon1PV <= 0){
        message.reply(`Egalité entre ` + pokemon1['pokemon_name'] + ` et ` + pokemon2['pokemon_name'] + ` !`);
      }
      if(!connu){
        message.reply(args[0] + ` n'existe pas !
        Combat Impossible !`);
      }
      if(!connu1){
        message.reply(args[1] + ` n'existe pas !
        Combat Impossible !`);
      }
    }else{
      message.reply(`Vous devez choisir deux pokémons pour le combat !`);
    }
  });
}
