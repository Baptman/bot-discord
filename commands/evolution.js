var unirest = require("unirest");

var req = unirest("GET", "https://pokemon-go1.p.rapidapi.com/pokemon_evolutions.json");

req.headers({
	"x-rapidapi-key": "d2a1cf1847msh7cb897edaba547bp1b6952jsnde9cc7dce47e",
	"x-rapidapi-host": "pokemon-go1.p.rapidapi.com",
	"useQueryString": true
});


module.exports = function(client, message, args){
  req.end(function (res) {
    let connu = false;
  	if (res.error) throw new Error(res.error);
    console.log(args);
    for(const pokemon of res.body){
      let evolutions = pokemon['evolutions'];
      let pokemon_evolutions = [];
      let pokemon_id = pokemon['pokemon_id'].toString();
      if(pokemon['pokemon_name'].toString().toLowerCase() === args.toString().toLowerCase() && !connu || pokemon_id == args && !connu){
        while(pokemon_id.length < 3){
          pokemon_id = "0" + pokemon_id;
        }
        for(const evolution of evolutions){
	  pok_id = evolution['pokemon_id'].toString();
	  while(pok_id.length < 3){
            pok_id = "0" + pok_id;
          }
	  pokemon_evolutions.push([pok_id,evolution['pokemon_name']]);
        }
        console.log(pokemon_id);
	console.log(pokemon_evolutions);
        message.reply("https://assets.pokemon.com/assets/cms2/img/pokedex/full/" + pokemon_id + ".png \n" +
	  pokemon['pokemon_name'] + ` est le pokémon n°` + pokemon_id + ` du pokédex !\n`
        );
		message.channel.send(`Il peut évoluer en`);
	for(let pokemon_evolution in pokemon_evolutions){
	  message.channel.send("\n- "+pokemon_evolutions[pokemon_evolution][1]+"\n"+ "https://assets.pokemon.com/assets/cms2/img/pokedex/full/" + pokemon_evolutions[pokemon_evolution][0] + ".png\n");
        }
	

      connu = true;
      }
    }
    if(!connu){
      message.reply(args + ` n'existe pas ou n'a pas d'évolutions !`);
    }
  });
}
