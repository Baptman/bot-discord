const pokemonTrad = require('pokemon');

const unirest = require("unirest");

const req = unirest("GET", "https://pokemon-go1.p.rapidapi.com/pokemon_stats.json");

const listLangue = ['de','en','fr','ja','ko','ru']

req.headers({
	"x-rapidapi-key": "d2a1cf1847msh7cb897edaba547bp1b6952jsnde9cc7dce47e",
	"x-rapidapi-host": "pokemon-go1.p.rapidapi.com",
	"useQueryString": true
});

module.exports = function(client, message, args){
  req.end(function (res) {
    let connuPokemon = false;
    let connuLangue = false;
  	if (res.error) throw new Error(res.error);
    console.log(args);
    if(args.length == 2){
      for(const pokemon of res.body){
        let pays = args[1].toString().toLowerCase();
        if (listLangue.includes(pays)){
          connuLangue = true;
        }
	if(connuLangue){
          let pokemon_id = pokemon['pokemon_id'].toString();
          if(pokemon['pokemon_name'].toString().toLowerCase() === args[0].toString().toLowerCase() && !connuPokemon || pokemon_id == args[0] && !connuPokemon){
            while(pokemon_id.length < 3){
              pokemon_id = "0" + pokemon_id;
            }
            console.log(pokemon_id);
	    let trad = pokemonTrad.getName(pokemon_id,pays);
            message.reply(
	      trad
            );
          connuPokemon = true;
          }
        }
      }
      if(connuLangue){
        if(!connuPokemon){
          message.reply(args[0] + ` n'existe pas !`);
        }
      }
      if(!connuLangue){
        message.reply(args[1] + ` est une langue qui n'existe pas !`);
      }
    }
    else{
      message.reply(`il faut choisir un pokemon et une langue !`);
    }
  });
}
